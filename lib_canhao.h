#include "lib_lista_tiro.h"

#define S_CANHAO "./*\\.M^W^M"
#define ALTURA_CANHAO 2
#define LARGURA_CANHAO 5

struct t_canhao {
	int posX;
	int posY;
	int vivo;
	t_lista_tiro *tiros;
};
typedef struct t_canhao t_canhao;


t_canhao *inicia_canhao();

void desenha_canhao(t_canhao *c);

void move_esquerda(t_canhao *c);

void move_direita(t_canhao *c);

void destroi_canhao(t_canhao *c);
