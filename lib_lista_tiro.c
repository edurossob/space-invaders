#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_lista_tiro.h"

void inicia_lista_tiro(t_lista_tiro *l){
  l->ini = malloc(sizeof(t_tiro_nodo));

  l->fim = malloc(sizeof(t_tiro_nodo));

  l->fim->prev = l->ini;
  l->ini->prox = l->fim;

  l->fim->prox = NULL;
  l->ini->prev = NULL;

  l->atual = NULL;

  l->tamanho = 0;
  l->cont = 0;
}

t_tiro_nodo* inicia_tiro(int posX, int id){
	t_tiro_nodo *n;
	n = malloc(sizeof(t_tiro_nodo));

	n->posX = posX + 2;
	n->posY = 35;
	n->id = id;

	return n;
}

int lista_tiro_vazia(t_lista_tiro *l){
  return l->tamanho == 0;
}

void insere_fim_lista_tiro(t_tiro_nodo* n, t_lista_tiro *l){
	n->prox = l->fim;
	n->prev = l->fim->prev;

	l->fim->prev->prox = n;
	l->fim->prev = n;

	l->tamanho++;
}

void atira_tiro(t_lista_tiro *l, int posX){
	t_tiro_nodo *tiro;

	l->cont ++;
	tiro = inicia_tiro(posX, l->cont);
	insere_fim_lista_tiro(tiro, l);
}

int tamanho_lista_tiro(t_lista_tiro *l){
	return l->tamanho;
}

void destroi_tiro(int id, t_lista_tiro *l){
	if(lista_tiro_vazia(l))
		return;

	t_tiro_nodo *trash;

  	while(l->atual != l->fim){
		trash = l->atual;
	    if(l->atual->id == id){
			l->atual->prev->prox = l->atual->prox;
			l->atual->prox->prev = l->atual->prev;
			l->tamanho--;
			free(trash);
			return;
		}
    l->atual = l->atual->prox;
	}
}

void move_tiro(t_tiro_nodo *n){
	n->posY--;
}


void move_tiros(t_lista_tiro *l){
	if(lista_tiro_vazia(l))
		return;

	l->atual = l->ini->prox;

	while(l->atual != l->fim){
		move_tiro(l->atual);
		if(l->atual->posY < 1){
			destroi_tiro(l->atual->id, l);
		}
		l->atual = l->atual->prox;
	}
}

void desenha_tiro(t_tiro_nodo *n){
	move(n->posY, n->posX);
	addch(S_TIRO[0]);
}

void desenha_tiros(t_lista_tiro *l){
	if(lista_tiro_vazia(l))
		return;

	l->atual = l->ini->prox;

	while(l->atual != l->fim){
		desenha_tiro(l->atual);
		l->atual = l->atual->prox;
	}
}