#include "lib_colisoes.h"
#include <ncurses.h>
#include <stdlib.h>

int verifica_tiro_alien(t_alien_nodo *a, t_tiro_nodo *t){
	int largura;
	if(a->sprite == 1 || a->sprite == 2)
		largura = LARGURA_ALIEN_1;
	else if(a->sprite == 3 || a->sprite == 4)
		largura = LARGURA_ALIEN_2;
	else if(a->sprite == 5 || a->sprite == 6)
		largura = LARGURA_ALIEN_3;

	if(t->posX >= a->posX && t->posX <= a->posX + largura)
		if(t->posY >= a->posY && t->posY <= a->posY + ALTURA_ALIEN -1)
				return 1;
	return 0;
}


int colisao_tiro_alien(t_lista_tiro *t, t_lista_alien *a){
	int score;
	score = 0;

	t->atual = t->ini->prox;
    while(t->atual != t->fim){
      	a->atual = a->ini->prox;
	    while(a->atual != a->fim){
	    	if(verifica_tiro_alien(a->atual, t->atual)){
	    		mata_alien(a->atual->id, a);
	    		destroi_tiro(t->atual->id, t);
	    		score += 50;
	    	}
	    	a->atual = a->atual->prox;
	    }
      t->atual = t->atual->prox;
    }

	return score;
}

int verifica_tiro_barreira(t_tiro_nodo *t, t_barreira_nodo *b){
	if((t->posX == b->posX))
		if((t->posY == b->posY))
			return 1;
	return 0;
}

int colisao_tiro_barreira(t_lista_tiro *t, t_lista_barreira *b){
	int score;
	score = 0;

	t->atual = t->ini->prox;
    while(t->atual != t->fim){
      	b->atual = b->ini->prox;
	    while(b->atual != b->fim){
	    	if(verifica_tiro_barreira(t->atual, b->atual)){
	    		quebra_barreira(b->atual->id, b);
	    		destroi_tiro(t->atual->id, t);
	    		score -= 10;
	    	}
	    	b->atual = b->atual->prox;
	    }
      t->atual = t->atual->prox;
    }
	
	return score;
}

int verifica_alien_barreira(t_alien_nodo *a, t_barreira_nodo *b){
	int largura;
	if(a->sprite == 1 || a->sprite == 2)
		largura = LARGURA_ALIEN_1;
	else if(a->sprite == 3 || a->sprite == 4)
		largura = LARGURA_ALIEN_2;
	else if(a->sprite == 5 || a->sprite == 6)
		largura = LARGURA_ALIEN_3;

	if(b->posX >= a->posX && b->posX <= a->posX + largura)
		if(b->posY >= a->posY && b->posY <= a->posY + ALTURA_ALIEN -1)
			return 1;

	
	return 0;
}

int colisao_alien_barreira(t_lista_alien *a, t_lista_barreira *b){
	int score;
	score = 0;

	a->atual = a->ini->prox;
    while(a->atual != a->fim){
	    		/*
	    		*/
      	b->atual = b->ini->prox;
	    while(b->atual != b->fim){
	    	if(verifica_alien_barreira(a->atual, b->atual)){
	    		quebra_barreira(b->atual->id, b);
	    		score -= 10;
	    	}
	    	b->atual = b->atual->prox;
	    }
      a->atual = a->atual->prox;
    }

	return score;
}

int verifica_tiro_mother(t_tiro_nodo *t, t_mother *m){
	if(t->posX >= m->posX && t->posX <= m->posX + LARGURA_MOTHER)
		if(t->posY >= m->posY && t->posY <= m->posY + ALTURA_MOTHER -1)
			return 1;
	return 0;
}

int colisao_tiro_mother(t_lista_tiro *t, t_mother *m){
	t->atual = t->ini->prox;
    while(t->atual != t->fim){
    	if(verifica_tiro_mother(t->atual, m)){
    		mata_mother(m);
    		destroi_tiro(t->atual->id, t);
    		return 500;
    	}
      t->atual = t->atual->prox;
    }
    return 0;
}

int verifica_bomba_barreira(t_bomba_nodo *b, t_barreira_nodo *n){
	if(b->posX == n->posX)
		if(b->posY == n->posY)
				return 1;
	return 0;
}

int colisao_bomba_barreira(t_lista_bomba *b, t_lista_barreira *n){
	b->atual = b->ini->prox;
    while(b->atual != b->fim){
      	n->atual = n->ini->prox;
	    while(n->atual != n->fim){
	    	if(verifica_bomba_barreira(b->atual, n->atual)){
	    		quebra_barreira(n->atual->id, n);
	    		destroi_bomba(b->atual->id, b);
	    	}
	    	n->atual = n->atual->prox;
	    }
      	b->atual = b->atual->prox;
    }

	return 0;
}

int verifica_bomba_canhao(t_bomba_nodo *b, t_canhao *c){
	if(b->posX >= c->posX && b->posX <= c->posX + LARGURA_CANHAO)
		if(b->posY >= c->posY && b->posY <= c->posY + ALTURA_CANHAO -1)
			return 1;
	return 0;
}

int colisao_bomba_canhao(t_lista_bomba *b, t_canhao *c){
	b->atual = b->ini->prox;
    while(b->atual != b->fim){
    	if(verifica_bomba_canhao(b->atual, c)){
    		destroi_canhao(c);
    		destroi_bomba(b->atual->id, b);
    		return 1;
    	}
      b->atual = b->atual->prox;
    }
    return 0;
}


int verifica_alien_canhao(t_alien_nodo *a, t_canhao *c){
	int largura;
	largura = 5;

	if(a->sprite == 1 || a->sprite == 2)
		largura = LARGURA_ALIEN_1;
	else if(a->sprite == 3 || a->sprite == 4)
		largura = LARGURA_ALIEN_2;
	else if(a->sprite == 5 || a->sprite == 6)
		largura = LARGURA_ALIEN_3;

	if(a->posX >= c->posX - largura && a->posX <= c->posX + LARGURA_CANHAO)
		if(a->posY + ALTURA_ALIEN >= c->posY)
			return 1;
	return 0;
}

int colisao_alien_canhao(t_lista_alien *a, t_canhao *c){
	a->atual = a->ini->prox;
    while(a->atual != a->fim){
		if(verifica_alien_canhao(a->atual, c)){
			destroi_canhao(c);
		}
    	a->atual = a->atual->prox;
    }
	return 0;
}

int colisao_alien_chao(t_lista_alien *a){
	a->atual = a->ini->prox;
    while(a->atual != a->fim){
		if(a->atual->posY + ALTURA_ALIEN == 38){
			return 1;
		}
    	a->atual = a->atual->prox;
    }
    return 0;
}

