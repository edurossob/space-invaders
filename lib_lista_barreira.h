#define ALTURA_BARREIRA 3
#define LARGURA_BARREIRA 7
#define S_BARREIRA " AMMMA MMMMMMMMM   MM"

struct t_barreira_nodo{
	int id;
	int posX;
	int posY;
	char tipo;
	struct t_barreira_nodo *prox;
	struct t_barreira_nodo *prev;
};
typedef struct t_barreira_nodo t_barreira_nodo;

struct t_lista_barreira{
	t_barreira_nodo *ini;
    t_barreira_nodo *atual;
    t_barreira_nodo *fim;
    int tamanho;
    int posX;
};
typedef struct t_lista_barreira t_lista_barreira;

void inicia_lista_barreira(t_lista_barreira *l, int posX);

t_barreira_nodo* inicia_barreira(int id, int posX, int posY, char tipo);

int lista_barreira_vazia(t_lista_barreira *l);

void inicia_bloco(t_lista_barreira *l);

void quebra_barreira(int id, t_lista_barreira *l);

void insere_fim_lista_barreira(t_barreira_nodo *n, t_lista_barreira *l);

void destroi_lista_barreira(t_lista_barreira *l);

void imprime_barreira(t_lista_barreira *l);

void desenha_barreiras(t_lista_barreira *b1, t_lista_barreira *b2, t_lista_barreira *b3, t_lista_barreira *b4);