#define S_MOTHER_1 "   ___     /O_O\\  0===H===0"
#define S_MOTHER_2 "   ___     />_<\\  0===H===0"
/*
   ___   
  /O_O\  
0===H===0 
   ___   
  />_<\  
0===H===0 
*/
#define ALTURA_MOTHER 3
#define LARGURA_MOTHER 9
#define VELOCIDADE_MOTHER 2
#define POS_INICIAL_MOTHER 100

struct t_mother{
	int posX;
	int posY;
	int existe;
};
typedef struct t_mother t_mother;

t_mother *inicia_mother();

void desenha_mother(t_mother *m);

void move_mother(t_mother *m, int cont);

void mata_mother(t_mother *m);

void destroi_mother(t_mother *m);

void invoca_mother(t_mother *m);
