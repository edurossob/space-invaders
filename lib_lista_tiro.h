#define S_TIRO "|"

struct t_tiro_nodo {
	int posX;
	int posY;
	int id;
    struct t_tiro_nodo *prox;
    struct t_tiro_nodo *prev;
};
typedef struct t_tiro_nodo t_tiro_nodo;

struct t_lista_tiro {
    t_tiro_nodo *ini;
    t_tiro_nodo *atual;
    t_tiro_nodo *fim;
    int tamanho;
    int cont;
};
typedef struct t_lista_tiro t_lista_tiro;

void inicia_lista_tiro(t_lista_tiro *l);

void atira_tiro(t_lista_tiro *l, int posX);

t_tiro_nodo* inicia_tiro(int posX, int id);

int tamanho_lista_tiro(t_lista_tiro *l);

void destroi_tiro(int id, t_lista_tiro *l);

void move_tiro(t_tiro_nodo *n);

void move_tiros(t_lista_tiro *l);

void insere_fim_lista_tiro(t_tiro_nodo* n, t_lista_tiro *l);

void desenha_tiro(t_tiro_nodo *n);

void desenha_tiros(t_lista_tiro *l);