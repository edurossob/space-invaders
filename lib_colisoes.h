
#include "lib_lista_alien.h"
#include "lib_canhao.h"
#include "lib_lista_barreira.h"
#include "lib_mother.h"

int verifica_tiro_alien(t_alien_nodo *a, t_tiro_nodo *t);

int colisao_tiro_alien(t_lista_tiro *t, t_lista_alien *a);

int verifica_tiro_barreira(t_tiro_nodo *t, t_barreira_nodo *b);
int colisao_tiro_barreira(t_lista_tiro *t, t_lista_barreira *b);
int colisao_alien_barreira(t_lista_alien *a, t_lista_barreira *b);

int colisao_tiro_mother(t_lista_tiro *t, t_mother *m);

int colisao_bomba_barreira(t_lista_bomba *b, t_lista_barreira *n);
int colisao_bomba_canhao(t_lista_bomba *b, t_canhao *c);

int colisao_alien_canhao(t_lista_alien *a, t_canhao *c);
int colisao_alien_chao(t_lista_alien *a);
