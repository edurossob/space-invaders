#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include "lib_lista_barreira.h"

/*struct t_barreira_nodo{
	int id;
	int posX;
	int posY;
	struct t_barreira_nodo *prox;
	struct t_barreira_nodo *prev;
};*/

void inicia_lista_barreira(t_lista_barreira *l, int posX){
  l->ini = malloc(sizeof(t_lista_barreira));

  l->fim = malloc(sizeof(t_lista_barreira));

  l->fim->prev = l->ini;
  l->ini->prox = l->fim;

  l->fim->prox = NULL;
  l->ini->prev = NULL;

  l->atual = NULL;
  l->posX = posX;

  l->tamanho = 0;
}


t_barreira_nodo* inicia_barreira(int id, int posX, int posY, char tipo){
	t_barreira_nodo *d;
	d = malloc(sizeof(t_barreira_nodo));

	d->id = id;
	d->posX = posX;
	d->posY = posY;
	d->tipo = tipo;

	return d;
}

int lista_barreira_vazia(t_lista_barreira *l){
	return l->tamanho == 0;
}

void inicia_bloco(t_lista_barreira *l){
	int i, j, cont;
	t_barreira_nodo *n;

	cont = 0;

	for(i = 0; i < ALTURA_BARREIRA; i++)
		for(j = 0; j < LARGURA_BARREIRA; j++){
			cont++;
			if(!(S_BARREIRA[i * LARGURA_BARREIRA + j] == ' ')){
				n = inicia_barreira(cont, l->posX + j, 30 + i, S_BARREIRA[i * LARGURA_BARREIRA + j]);
				insere_fim_lista_barreira(n, l);
			}
		}
}

void quebra_barreira(int id, t_lista_barreira *l){
  if(lista_barreira_vazia(l))
    return;

  t_barreira_nodo *trash;

  l->atual = l->ini->prox;
  while(l->atual != l->fim){
    trash = l->atual;
    if(l->atual->id == id){
      l->atual->prev->prox = l->atual->prox;
      l->atual->prox->prev = l->atual->prev;
      l->tamanho--;

      free(trash);
      return;
    }
    l->atual = l->atual->prox;
  }
}
	

void insere_fim_lista_barreira(t_barreira_nodo *n, t_lista_barreira *l){
  n->prox = l->fim;
  n->prev = l->fim->prev;

  l->fim->prev->prox = n;
  l->fim->prev = n;

  l->tamanho++;
}

void destroi_lista_barreira(t_lista_barreira *l){
	
}

void imprime_barreira(t_lista_barreira *l){
	if(lista_barreira_vazia(l))
		return;

	l->atual = l->ini->prox;

	while(l->atual != l->fim){
		move(l->atual->posY, l->atual->posX);
		addch(l->atual->tipo);
		l->atual = l->atual->prox;
	}
}

void desenha_barreiras(t_lista_barreira *b1, t_lista_barreira *b2, t_lista_barreira *b3, t_lista_barreira *b4){
	imprime_barreira(b1);
	imprime_barreira(b2);
	imprime_barreira(b3);
	imprime_barreira(b4);
}