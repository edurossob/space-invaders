#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include <string.h>
#include "lib_colisoes.h"

void desenha_tabuleiro(char score[5]){
	int i;

	for(i = 1; i < 122; i++){
		move(0, i);
		addch('_');
		move(37, i);
		addch('_');
	}
	for(i = 1; i < 38; i++){	
		move(i, 0);
		addch('|');
		move(i, 100);
		addch('|');
		move(i, 122);
		addch('|');
	}

	/*     "012345678901234567890" */
	move(1, 101);
	addstr(". .   .   .   .   . .");
	move(2, 101);
	addstr(" .  .   .   .   .  . ");
	move(3, 101);
	addstr(".  .  S P A C E  .  .");
	move(4, 101);
	addstr(" . I N V A D E R S . ");
	move(5, 101);
	addstr("_____________________");
	move(7, 101);
	addstr("  SCORE :  ");
	move(7, 112);
	addstr(score);

	move(33, 101);
	addstr("_____________________");
	move(34, 101);
	addstr("Feito por:");
	move(35, 101);
	addstr("Eduardo Rosso B.");
	move(36, 101);
	addstr("GRR 20190378");

}

int inicia_tela(){
	int nlin, ncol;

	initscr();
  	getmaxyx(stdscr, nlin, ncol);

  	if(nlin < 37 || ncol < 100){
  		endwin();
  		printf("o terminal deve ter no mínimo 37 linhas por 100 colunas %d %d\n",nlin, ncol);
  		return 0;
  	}

    cbreak();               /* desabilita o buffer de entrada */
    noecho();               /* não mostra os caracteres digitados */
    nodelay(stdscr, TRUE);  /* faz com que getch não aguarde a digitação */
    keypad(stdscr, TRUE);   /* permite a leitura das setas */
    curs_set(FALSE);        /* não mostra o cursor na tela */

  	return 1;
}

void atira(t_canhao *c, t_lista_tiro *t){
	if (tamanho_lista_tiro(t) > 2)
		return;
	atira_tiro(t, c->posX);
}


int main(){

	if(!inicia_tela())
		return 0;
	
	/* CANHAO */
	t_canhao *c;
  	c = inicia_canhao();

  	/* ALIENS */
  	t_lista_alien aliens;
  	inicia_lista_alien(&aliens);
	inicia_pelotao(&aliens);

	/* TIROS */
	t_lista_tiro tiros;
	inicia_lista_tiro(&tiros);

	/* BOMBAS */
	t_lista_bomba bombas;
	inicia_lista_bomba(&bombas);

	/* BARREIRAS */
	t_lista_barreira barr_1;
	inicia_lista_barreira(&barr_1, 15);
	
	t_lista_barreira barr_2;
	inicia_lista_barreira(&barr_2, 37);
	
	t_lista_barreira barr_3;
	inicia_lista_barreira(&barr_3, 59);
	
	t_lista_barreira barr_4;
	inicia_lista_barreira(&barr_4, 81);

	inicia_bloco(&barr_1);
	inicia_bloco(&barr_2);
	inicia_bloco(&barr_3);
	inicia_bloco(&barr_4);

	/* MOTHER SHIP */
	t_mother *m;
	m = inicia_mother();

	/* JOGO */

	long cont;
	int key;
	int score;
	char sscore[5];

	score = 0;
	cont = 0;
	
	while (c->vivo && aliens.tamanho){
		key = getch();
		if(key == ' ' || key == 'w' || key == KEY_UP) {
			atira(c, &tiros);
		}
		else
		if(key == KEY_LEFT || key == 'a') {
			move_esquerda(c);
		}
		else if (key == KEY_RIGHT || key == 'd') {
			move_direita(c);
		}
		else if (key == 'q') {
			endwin();
			exit(0);
		}
		else if(key == 'e'){
			atira_bomba(&bombas, 30, 9);
		}

		invoca_mother(m);

		move_tiros(&tiros);
		move_bombas(&bombas, cont);
		move_mother(m, cont);
		rotina_aliens(&aliens, cont, &bombas); 
		/*move e atira*/

		score += colisao_tiro_alien(&tiros, &aliens);
		score += colisao_tiro_mother(&tiros, m);
		sprintf(sscore, "%d", score);
		
		score += colisao_tiro_barreira(&tiros, &barr_1);
		score += colisao_tiro_barreira(&tiros, &barr_2);
		score += colisao_tiro_barreira(&tiros, &barr_3);
		score += colisao_tiro_barreira(&tiros, &barr_4);

		colisao_alien_barreira(&aliens, &barr_1);
		colisao_alien_barreira(&aliens, &barr_2);
		colisao_alien_barreira(&aliens, &barr_3);
		colisao_alien_barreira(&aliens, &barr_4);

		colisao_bomba_barreira(&bombas, &barr_1);
		colisao_bomba_barreira(&bombas, &barr_2);
		colisao_bomba_barreira(&bombas, &barr_3);
		colisao_bomba_barreira(&bombas, &barr_4);

		colisao_bomba_canhao(&bombas, c);

		colisao_alien_canhao(&aliens, c);
		if(colisao_alien_chao(&aliens))
			destroi_canhao(c);

		atualiza_velocidade_aliens(&aliens);

		erase();
		wattron(stdscr, COLOR_PAIR(1));
		desenha_tabuleiro(sscore);
		
		wattron(stdscr, COLOR_PAIR(2));
		desenha_canhao(c);
		
		wattron(stdscr, COLOR_PAIR(3));
		imprime_aliens(&aliens);
		
		wattron(stdscr, COLOR_PAIR(4));
		desenha_tiros(&tiros);

		wattron(stdscr, COLOR_PAIR(5));
		desenha_barreiras(&barr_1, &barr_2, &barr_3, &barr_4);

		wattron(stdscr, COLOR_PAIR(6));
		desenha_bombas(&bombas);
		
		wattron(stdscr, COLOR_PAIR(7));
		desenha_mother(m);
		
		refresh();
		usleep(33500);

		cont ++;
	}

	move(16, 35);
	addstr("___________________________________");
	move(17, 35);
	addstr("                                   ");
	move(18, 35);
	addstr("             - ACABO -             ");
	move(19, 35);
	if(c->vivo)
		addstr("       !!! e tu GANHASTE !!!  :)     ");
	else
		addstr("         - e tu PERDESTE -   :(      ");
	move(20, 35);
	addstr("       press 'q' to continue       ");
	move(21, 35);
	addstr("___________________________________");
	move(22, 35);
	addstr("SCORE FINAL:                       ");
	move(22, 48);
	addstr(sscore);
	move(23, 35);
	addstr("___________________________________");


	refresh();

	while(1){
		key = getch();
		if(key == 'q') {
			endwin();
			exit(0);
		}
		usleep(33500);
	}

	return 1;
}
