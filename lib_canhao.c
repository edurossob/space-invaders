#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_canhao.h"


t_canhao* inicia_canhao(){
	t_canhao *c;
	c = malloc(sizeof(t_canhao));

	c->posX = 60;
	c->posY = 35;
	c->vivo = 1;

	t_lista_tiro *t;
	t = malloc(sizeof(t_lista_tiro));

	c->tiros = t;

	return c;
}

void desenha_canhao(t_canhao* c){
	int i, j;

	for(i = 0; i< ALTURA_CANHAO; i++)
		for(j = 0; j< LARGURA_CANHAO; j++){
			move(c->posY + i, c->posX + j);
			addch(S_CANHAO[LARGURA_CANHAO *i +j]);	
		}
}

void move_esquerda(t_canhao* c){
	if(c->posX > 1)
		c->posX--;
}

void move_direita(t_canhao* c){
	if(c->posX < 99 - LARGURA_CANHAO)
		c->posX++;
}

void destroi_canhao(t_canhao *c){
	c->vivo=0;
}
