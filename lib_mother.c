#include <stdlib.h>
#include <time.h>
#include <ncurses.h>
#include "lib_mother.h"

t_mother *inicia_mother(){
	t_mother *m;
	m = malloc(sizeof(t_mother));

	m->posX = POS_INICIAL_MOTHER;
	m->posY = 1;
	m->existe = 0;

	return m;
}

void desenha_mother(t_mother *m){
	int i, j;
	float r;
	srand(time(0));
	r = rand()%4;

	if(!(m->posX > 99)){
		for(i = 0; i< ALTURA_MOTHER; i++)
			for(j = 0; j< LARGURA_MOTHER; j++){
				if(m->posX + j > 0 && m->posX + j < 100){
					move(m->posY + i, m->posX + j);
					if(r < 1)
						addch(S_MOTHER_2[LARGURA_MOTHER *i +j]);	
					else
						addch(S_MOTHER_1[LARGURA_MOTHER *i +j]);	

				}
		}
	}
}

void move_mother(t_mother *m, int cont){
	if(!(cont % VELOCIDADE_MOTHER))
		if(m->existe){
			m->posX--;
			if(m->posX + LARGURA_MOTHER < 0 ){
				mata_mother(m);
			}
		}
}

void mata_mother(t_mother *m){
	m->existe = 0;
	m->posX = POS_INICIAL_MOTHER;
}

void destroi_mother(t_mother *m){

}

void invoca_mother(t_mother *m){
	float r;
	if(!m->existe){
		srand(time(0));
		r = rand()%11;
		if(r < 2){
			m->existe = 1;
		}
	}
}