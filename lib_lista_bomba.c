#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include "lib_lista_bomba.h"

void inicia_lista_bomba(t_lista_bomba *l){
  l->ini = malloc(sizeof(t_bomba_nodo));

  l->fim = malloc(sizeof(t_bomba_nodo));

  l->fim->prev = l->ini;
  l->ini->prox = l->fim;

  l->fim->prox = NULL;
  l->ini->prev = NULL;

  l->atual = NULL;

  l->tamanho = 0;
  l->cont = 0;
}

int lista_bomba_vazia(t_lista_bomba *l){
  return l->tamanho == 0;
}

t_bomba_nodo* inicia_bomba(int posX, int posY, int id){
	t_bomba_nodo *n;
	n = malloc(sizeof(t_bomba_nodo));

	n->posX = posX + 2;
	n->posY = posY + 2;
	n->id = id;

	return n;
}

void insere_fim_lista_bomba(t_bomba_nodo* n, t_lista_bomba *l){
	n->prox = l->fim;
	n->prev = l->fim->prev;

	l->fim->prev->prox = n;
	l->fim->prev = n;

	l->tamanho++;
}

void atira_bomba(t_lista_bomba *l, int posX, int posY){
	srand(time(NULL));
	if(!(rand()%2)){
		t_bomba_nodo *bomba;

		l->cont ++;
		bomba = inicia_bomba(posX, posY, l->cont);
		insere_fim_lista_bomba(bomba, l);
	}
}

int tamanho_lista_bomba(t_lista_bomba *l){
	return l->tamanho;
}

void destroi_bomba(int id, t_lista_bomba *l){
	if(lista_bomba_vazia(l))
		return;

	t_bomba_nodo *trash;

  	while(l->atual != l->fim){
		trash = l->atual;
	    if(l->atual->id == id){
			l->atual->prev->prox = l->atual->prox;
			l->atual->prox->prev = l->atual->prev;
			l->tamanho--;
			free(trash);
			return;
		}
    l->atual = l->atual->prox;
	}
}

void move_bomba(t_bomba_nodo *n){
	n->posY++;
}


void move_bombas(t_lista_bomba *l, int cont){
	if(!(cont % VELOCIDADE_BOMBA)){
		l->atual = l->ini->prox;

		while(l->atual != l->fim){
			move_bomba(l->atual);
			if(l->atual->posY > 36){
				destroi_bomba(l->atual->id, l);
			}
			l->atual = l->atual->prox;
		}
	}
}

void desenha_bomba(t_bomba_nodo *n){
	move(n->posY, n->posX);
	addch(S_BOMBA[0]);
}

void desenha_bombas(t_lista_bomba *l){
	if(lista_bomba_vazia(l))
		return;

	l->atual = l->ini->prox;

	while(l->atual != l->fim){
		desenha_bomba(l->atual);
		l->atual = l->atual->prox;
	}
}