#define S_BOMBA "!"
#define VELOCIDADE_BOMBA 5

struct t_bomba_nodo {
	int posX;
	int posY;
	int id;
	struct t_bomba_nodo *prox;
	struct t_bomba_nodo *prev;
};
typedef struct t_bomba_nodo t_bomba_nodo;

struct t_lista_bomba {
    t_bomba_nodo *ini;
    t_bomba_nodo *atual;
    t_bomba_nodo *fim;
    int tamanho;
    int cont;
};
typedef struct t_lista_bomba t_lista_bomba;

void inicia_lista_bomba(t_lista_bomba *l);

void atira_bomba(t_lista_bomba *l, int posX, int posY);

t_bomba_nodo* inicia_bomba(int posX, int posY, int id);

int tamanho_lista_bomba(t_lista_bomba *l);

void destroi_bomba(int id, t_lista_bomba *l);

void move_bomba(t_bomba_nodo *n);

void move_bombas(t_lista_bomba *l, int cont);

void insere_fim_lista_bomba(t_bomba_nodo* n, t_lista_bomba *l);

void desenha_bomba(t_bomba_nodo *n);

void desenha_bombas(t_lista_bomba *l);