#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <time.h>
#include "lib_lista_alien.h"

void inicia_lista_alien(t_lista_alien *l){
  l->ini = malloc(sizeof(t_alien_nodo));

  l->fim = malloc(sizeof(t_alien_nodo));

  l->fim->prev = l->ini;
  l->ini->prox = l->fim;

  l->fim->prox = NULL;
  l->ini->prev = NULL;

  l->atual = NULL;

  l->dir = 0;
  l->velocidade = 0;
  l->batidas_parede = -1;

  l->tamanho = 0;
}

t_alien_nodo* inicia_alien(int tipo, int id, int posX, int posY){
  t_alien_nodo *d;
  d = malloc(sizeof(t_alien_nodo));

  d->posX = posX;
  d->posY = posY;
  d->id = id;

  if(tipo == 1)
    d->sprite = 1;

  if(tipo == 2)
    d->sprite = 3;

  if(tipo == 3)
  	d->sprite = 5;

  return d;
}

int lista_alien_vazia(t_lista_alien *l){
  return l->tamanho == 0;
}

/*
void destroi_lista_alien(t_lista_alien *l){

  while(!lista_alien_vazia(l))
    mata_alien(l);

  free(l->fim);
  free(l->ini);
  l->ini = NULL;
  l->fim = NULL;
  l->atual = NULL;
}
*/

int insere_fim_lista_alien(t_alien_nodo *n, t_lista_alien *l){
  n->prox = l->fim;
  n->prev = l->fim->prev;

  l->fim->prev->prox = n;
  l->fim->prev = n;

  l->tamanho++;
  return 1; 
}

void inicia_pelotao(t_lista_alien *l){
  int i, j, cont, tipo;
  t_alien_nodo *alien;

  cont = 0;
  tipo = 1;

  for(j = 0; j < 5; j++)
    for(i = 0; i < 11; i++){
		alien = inicia_alien(tipo, cont, i*6 + 1, j*4 + 4);
		insere_fim_lista_alien(alien, l);
		cont++;

		if(cont > 21)
			tipo = 2;

		if(cont > 43)
			tipo = 3;
    }
}

int mata_alien(int id, t_lista_alien* l){
  if(lista_alien_vazia(l))
    return 0;

  t_alien_nodo *trash;

  l->atual = l->ini->prox;
  while(l->atual != l->fim){
    trash = l->atual;
    if(l->atual->id == id){
      l->atual->prev->prox = l->atual->prox;
      l->atual->prox->prev = l->atual->prev;
      l->tamanho--;

      free(trash);
      return 1;
    }
    l->atual = l->atual->prox;
  }
  return 0;
}

int imprime_alien(t_alien_nodo* alien){
  int x, y;

  for(x = 0; x< ALTURA_ALIEN; x++)
    for(y = 0; y< LARGURA_ALIEN; y++){
      move(alien->posY + x, alien->posX + y);

      if(alien->sprite == 1)
        addch(S_A1_1[(LARGURA_ALIEN*(x)) + y]);
      else
      if(alien->sprite == 2)
        addch(S_A1_2[(LARGURA_ALIEN*(x)) + y]);
      else
      if(alien->sprite == 3)
        addch(S_A2_1[(LARGURA_ALIEN*(x)) + y]);
      else
      if(alien->sprite == 4)
        addch(S_A2_2[(LARGURA_ALIEN*(x)) + y]);   
      else
      if(alien->sprite == 5)
        addch(S_A3_1[(LARGURA_ALIEN*(x)) + y]);   
    }
  return 1;
}

void imprime_aliens(t_lista_alien *l){
	l->atual = l->ini->prox;
	while(l->atual != l->fim){
		imprime_alien(l->atual);
		l->atual = l->atual->prox;
	}
}

int verifica_direcao(t_lista_alien *l){
  l->atual = l->ini->prox;

  while(l->atual != l->fim){
    
    if(l->atual->posX == 1){
      l->dir = 1;
      l->batidas_parede++;
      return 1;
    }

    if(l->atual->posX == 95){
      l->dir = -1;
      l->batidas_parede++;
      return 1;
    }
    l->atual = l->atual->prox;
  }
  return 0;
}

void move_alien(t_lista_alien *l, t_alien_nodo *n, int cont){
	if(!(cont % (VELOCIDADE_MIN - l->velocidade))){
		n->posX += l->dir;
	}
}

void desce_aliens(t_lista_alien *l, int cont){
	if(!(cont % (VELOCIDADE_MIN - l->velocidade))){
		l->atual = l->ini->prox;
		while(l->atual != l->fim){
			l->atual->posY++; 
			l->atual = l->atual->prox;
		}
	}
}

void rotina_aliens(t_lista_alien *l, int cont, t_lista_bomba *b){
	if(verifica_direcao(l)){
		desce_aliens(l, cont);
	}
	srand(time(0));

	l->atual = l->ini->prox;
	while(l->atual != l->fim){
		move_alien(l, l->atual, cont);
		if(rand() % (8*l->tamanho - 1) < 2)
			atira_bomba(b, l->atual->posX, l->atual->posY);
		l->atual = l->atual->prox;
	}

}

void aumenta_velocidade(t_lista_alien *l){
  	if(l->velocidade < VELOCIDADE_MIN-1)
  		l->velocidade++;
  }

void atualiza_velocidade_aliens(t_lista_alien *l){
	if((l->velocidade == 0))
		if((l->batidas_parede > 0))
			aumenta_velocidade(l);

	if((l->velocidade == 1))
		if((l->batidas_parede > 4))
			aumenta_velocidade(l);

	if((l->batidas_parede > 15))
		aumenta_velocidade(l);
	
	if(l->tamanho <= 22)
		if((l->batidas_parede < 3)){
			aumenta_velocidade(l);
		}
}