#include "lib_lista_bomba.h"

/* Todo alien tem o padrão altura 3 largura 5,
 * Padrão utilizado apenas para posicionamento */
#define ALTURA_ALIEN 3
#define LARGURA_ALIEN 5

/* largura de cada tipo de alien */
#define LARGURA_ALIEN_1 5
#define LARGURA_ALIEN_2 3
#define LARGURA_ALIEN_3 3

#define VELOCIDADE_MIN 5
/*------- Padrão -> S_[tipo do alien]_[movimento] --------*/

/* sprite == 1 => S_A2_1 */
#define S_A1_1 "/===\\X-_-X\\___/"

/* sprite == 3 => S_A1_1 */
#define S_A2_1 " /^\\  \\o/  /Y\\ "

/*sprite == 5 => S_A3_1*/
#define S_A3_1 "  M   III NNNNN"


struct t_alien_nodo {
    int sprite;
    int id;
    int posY;
    int posX;
    struct t_alien_nodo *prox;
    struct t_alien_nodo *prev;
};
typedef struct t_alien_nodo t_alien_nodo;

struct t_lista_alien {
    t_alien_nodo *ini;
    t_alien_nodo *atual;
    t_alien_nodo *fim;
    int dir;
    int velocidade;
    int tamanho;
    int batidas_parede;
};
typedef struct t_lista_alien t_lista_alien;

/*
  Cria uma lista vazia. Ela eh duplamente encadeada e tem sentinelas no
  inicio e no final. Tambem tem um apontador para um elemento qualquer.
*/
void inicia_lista_alien(t_lista_alien *l);

t_alien_nodo* inicia_alien(int tipo, int id, int posX, int posY);

void inicia_pelotao(t_lista_alien *l);

/*Remove o alien que tenha o id id da lista
Retorna 1 se conseguiu, 0 caso contrario*/
int mata_alien(int id, t_lista_alien* l);

/*
  Remove todos os elementos da lista e faz com que ela aponte para NULL.
*/
void destroi_lista_alien(t_lista_alien *l);

/*
  Insere o elemento n no início da lista.
  Retorna 1 se a operação foi bem sucedida e zero caso contrário.
*/
int insere_fim_lista_alien(t_alien_nodo *n, t_lista_alien *l);

int lista_alien_vazia(t_lista_alien *l);

int imprime_alien(t_alien_nodo *n);

void imprime_aliens(t_lista_alien *l);

void move_alien(t_lista_alien *l, t_alien_nodo *a, int cont);

void rotina_aliens(t_lista_alien *l, int cont, t_lista_bomba *b);

void aumenta_velocidade(t_lista_alien *l);

void atualiza_velocidade_aliens(t_lista_alien *l);